import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";

export default function Courses() {

    // Checks if the mock data was captured.
    console.log(coursesData);
    console.log(coursesData[0]);

    // The course in the CourseCard component is called a 'prop' which is a shorthand for 'property' since components are considered as objects in React.js.

    // The curly braces ({}) are used for props to signify that we are providing information using JS expression.

    // We can pass information from one component to another using props. This is referred to as 'props drilling'.

    // The 'map' method loops through the individual course object in out array and returns a component for each courses.

    const courses = coursesData.map(course => {
        return (
            <CourseCard key = {course.id} course = {course} />
        );
    })

    return(
        <>
        {courses}
        </>

    )
}