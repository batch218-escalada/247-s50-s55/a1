import AppNavbar from './components/AppNavbar';
import { Container } from 'react-bootstrap';
import Courses from './pages/Courses';
import Home from './pages/home'
import Register from './pages/Register';
import Login from './pages/Login';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Logout from './pages/Logout';
import Error from "./pages/Error";
import './App.css';

import { useState } from 'react';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({ email : localStorage.getItem('email') })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Common patter in react.js for a component to return multiple elements.
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar />
    <Container>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/courses' element={<Courses />} />
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path='/logout' element={<Logout />} />
        <Route path='/*' element={<Error />} />
      </Routes>
    </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
