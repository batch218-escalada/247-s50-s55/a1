import { Card, Button } from "react-bootstrap";
import propTypes from "prop-types";
import { useState, useEffect } from "react";

export default function CourseCard({course}) {

    // Checks to see if the data was successfully passed
    // console.log(props)
    // Every component receives information in a form of an object
    // console.log(typeof props)

    // Destructure the 'course' properties into their own variables 'course' to make the code even shorter.

    const {name, description, price, id} = course;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

    // Syntax
        // const [getter, setter] = useState(initialGetterValue);

    const [count, setCount] = useState(0);
    const [seats, setSeat] = useState(30);
    const [isOpen, setIsOpen] = useState(true);

    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.

    console.log(useState(0));

    function enroll() {
        setCount(count + 1);
        console.log('Enrollees ' + count);
        setSeat(seats - 1);
        console.log('Seats: ' + seats)
    }

    // useEffect  allows us to instruct the app that the components needs to do something after render.

    useEffect(() => {
        if (seats === 0 ){
          setIsOpen(false);
          document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
          alert("No more seats available.")
        }
          //will run anytime one of the values in the array of dependencies changes.
      }, [seats]);
  

        // Three types of dependencies
            // No dependency - effect function will run every times the components renders.
            // With dependency (empty array) - effect function will only run (one time) when the components mounts and unmounts.
            // With dependency - effect function will run any time one of the values in the array of dependency changes.
    
    return(

        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Button id={`#btn-enroll-${id}`} className="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    );
}

    // Check if the CourseCard component is getting the correct prop types
    // PropTypes are used for validation information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

    CourseCard.propTypes = {
        course: propTypes.shape({
            name: propTypes.string.isRequired,
            description: propTypes.string.isRequired,
            price: propTypes.string.isRequired
        })
    }