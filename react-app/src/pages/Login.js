import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

import { useNavigate, Navigate } from "react-router-dom";
import UserContext from '../UserContext';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(false);

    function userLogin(e) {

        e.preventDefault();

        // Set the email of the authenticated user in the local storage.
            // Syntax
            // localStorage.setItem('propertyName', value)
            
        localStorage.setItem('email', email);

        setUser({ email: localStorage.getItem('email') });

        setEmail('');
        setPassword('');
        // navigate('/');

        alert('You are now logged in');

    }

    useEffect(() => {

        if (password === '' || email.length === '') {
            setIsActive(false);
            return;
        }

        setIsActive(true);

    }, [email, password]);

    return (
        (user.email !== null) ?
            <Navigate to='/courses' />
        :

        <Form onSubmit={(e) => userLogin(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
                    value={email}
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type='password' 
	                placeholder='Password' 
	                required
                    value={password}
                    onChange={e => {
                        setPassword(e.target.value);
                    }}
                />
            </Form.Group>


			{ isActive ? 

				<Button variant='success my-3' type='submit' id='submitBtn'>Login</Button>
				:
				<Button variant='secondary my-3' type='submit' id='submitBtn' disabled>Login</Button>

			}

        </Form>
    )
}